import {message} from "antd";
import {fileSuffix} from "../utils/common-data";

export function checkUrl(val) {
    if (!val) {
        message.error("输入内容不能为空！");
        return false;
    }else {
        let reg = /^((https|http|ftp|rtsp|mms)?:\/\/)[^\s]+/;
        if(!(reg.test(val))){
            message.error("请输入正确的file url！");
            return false;
        }
        const fileStrArr = val.split(".");
        const suffix = fileStrArr[fileStrArr.length - 1];

        let result = fileSuffix.some((item) => {
            return item === suffix
        });

        if (!result) {
            message.error("不支持该文件类型");
            return false;
        }
    }
    return true;
}

export function checkFile(val) {
    // 此处判断文件类型，以及文件大小  "MB";
    const size = (Number(val.size) / (1024 * 1024)).toFixed(2);
    const fileName = val.name;
    const fileStrArr = fileName.split(".");
    const suffix = fileStrArr[fileStrArr.length - 1];

    let result = fileSuffix.some((item) => {
        return item === suffix
    });

    if (!result) {
        message.error("不支持该文件类型");
        return false;
    }

    if (size > 10){
        message.error("文件不能大于10MB");
        return false;
    }
    return true;
}
